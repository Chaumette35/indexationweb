# Expension de Requêtes et Ranking

Ce projet est un index développé en Python, conçu pour analyser les informations obtenues par un crawler. 

### Lancer le projet

Pour effectuer des requêtes à partir de documents et d'index, il faut avoir deux fichiers json : 
 - un avec les informations sur les documents (URL et titre), 
 - l'autre contiendra le résultat d'un index minimal. 

Ensuite, vous pouvez lancer le projet avec le fichier `main.py`. Pour renseigner votre requête, vous devez l'inscrire dans le paramètre `query`. Vous pouvez changer le filtre (soit garder les documents qui ont tous les tokens de la requête, soit garder les documents qui ont au moins un token de la requête) en l'écrivant dans le paramètre `filter` (Valeurs possibles = et/ou, par défaut = "et"). La méthode de ranking est également modifiable en définissant le paramètre `rank` : 
 - avec "stopwords", les stop words auront un poids beaucoup moins important (4 fois moins). 
 - Avec "count", les tokens ont tous la même importance. 
 - Enfin, avec "bm25", les documents seront triés selon le score BM25. 

Cela donne donc : 

```bash
python3 main.py --query="Ma requete" --filter="ou" --rank="bm25"
```

### Fonctionnement du projet

Le but du projet est de filtrer l'ensemble des index et appliquer un ranking sur les documents qui y sont liés. 

 - Tout d'abord, il faut tokeniser la requête de la même manière que pour obtenir le fichier des index. Ici, nous appliquons uniquement une séparation par rapport aux espaces et une lowersation. 
 - Ensuite, nous filtrons les documents en fonction du paramètre `filter` (voir plus haut pour le détail du filtre). 
 - Puis, nous appliquons un ranking (définit par l'utilisateur) sur les documents : plus un document contient les tokens de la requête, plus le document sera haut. Les stop words auront un poids moins important lorsque le paramètre `rank`="stopwords". Lorsque `rank`="bm25", les documents seront ordonnés selon le score BM25. Ce score permet d'avoir un ranking plus fin des documents.  
 - Enfin, nous sauvegardons dans un fichier `results.json` l'URL et le titre des documents (selon notre ranking). Nous sauvegardons également le nombre total de documents et le nombre de documents après le filtrage. 

### Contributeur

Maël CHAUMETTE