import click

from app.query import Query


@click.command()
@click.option("--query", default = "")
@click.option("--filter", default = "et", help = "Valeurs possibles (et/ou)")
@click.option("--rank", default = "count", help = "Valeurs possibles (count/stopwords/bm25)")
def main(query, filter, rank):
    if filter not in ["et", "ou"]:
        raise("Wrong value for filter. (et/ou)")

    if rank not in ["count", "stopwords", "bm25"]:
        raise("Wrong value for rank. (count/stopwords)")

    query_obj = Query(
        "data/documents.json", 
        "data/title_pos_index.json", 
    )

    print("Début de l'analyse de votre requête\n-----------------------------------\n")

    query_obj.filter_documents(query, filter)
    query_obj.rank_documents(rank)
    query_obj.compute_query_results()
    query_obj.write_results("results.json")

    print("Enregistrement des résultats dans le fichier results.json")




if __name__ == "__main__":
    main()