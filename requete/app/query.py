import json

from app.utils import Utils


class Query:
    """
    Classe gérant l'expension et l'analyse d'une requête faite par un utilisateur

    Attributes
    ----------
    documents: Liste des documents récupérés par un crawler
        list[dict]
    index: Liste des index récupérés par un index minimal
        list[dict]
    filtered_documents: Dictionnaire où la clé est l'id d'un document gardé après filtrage 
    et la valeur est une liste de dictionnaire caractérisant un token de la requête
        dict
    query_results: Liste contenant les documents gardés après filtrage et triés d'après un ranking. 
    La liste contient également des métadonnées sur la requête telles que le nombre de documents 
    et le nombre de documents après filtrage. 
        list[dict]
    param_bm25: Dictionnaire avec les paramètres utilisant pour calculer le score BM25
        dict
    """
    def __init__(self, document_path, index_path):
        """
        Initializer

        Parameters
        ----------
        document_path: Chemin vers le fichier contenant les documents
            str
        index_path: Chemin vers le fichier contenant les index
            str
        """
        self.documents = self.init_documents(document_path)
        self.index = self.init_index(index_path)
        self.filtered_documents = {}
        self.query_results = []
        self.param_bm25 = {
            "constant": {"N": len(self.documents), "k1": 1.2, "b": 0.75}, 
            "tokens": {}, 
            "documents": {}
        }


    def init_documents(self, document_path):
        """
        Lit le contenu du fichier document_path

        Parameters
        ----------
        document_path: Chemin vers le fichier contenant les documents
            str
        
        Returns
        -------
            list[dict]
        """
        with open(document_path, "r") as f:
            data_doc = json.load(f)

        return data_doc


    def init_index(self, index_path):
        """
        Lit le contenu du fichier index_path

        Parameters
        ----------
        index_path: Chemin vers le fichier contenant les index
            str
        
        Returns
        -------
            list[dict]
        """
        with open(index_path, "r") as f:
            data_index = json.load(f)

        return data_index


    def filter_documents(self, query, filter):
        """
        Filtre les documents selon un filtre (OU/ET) sur la requête. 
        Si le filtre est OU alors on garde tous les documents avec au moins un des tokens de la requête. 
        Si le filtre est ET alors on garde les documents avec tous les tokens de la requête. 

        Parameters
        ----------
        query: Requête faite par l'utilisateur
            str
        filter: Filtre à appliquer sur la requête
            str

        Returns
        -------
            list
        """
        # Tous d'abord, on récupère l'id des documents avec les tokens
        doc_ids = {}
        for token_query in Utils.tokenize(query): # La fonction transforme la requête
            if token_query in self.index.keys():
                doc_ids[token_query] = set(self.index[token_query].keys())

                # Pour gagner du temps, on calcule le nombre de documents possèdant le token q_i
                if self.param_bm25["tokens"].get(token_query):
                    self.param_bm25["tokens"][token_query] += len(doc_ids[token_query])
                else:
                    self.param_bm25["tokens"][token_query] = 0

        if doc_ids:
            res = list(doc_ids.values())[0]
            for ids in list(doc_ids.values())[1:]:
                # L'opération sur l'id des documents dépend du filtre
                if filter == "et":
                    res = res.intersection(ids)
                else:
                    res = res.union(ids)

        # Enfin, on sauvegarde les résultats dans un dictionnaire où la clé est l'id du document et la valeur est
        # la liste des tokens avec des informations (positions, compte)
        for doc_id in res:
            self.filtered_documents[doc_id] = [
                {
                    "token": token_query, 
                    "positions": self.index[token_query][doc_id]["positions"], 
                    "count": self.index[token_query][doc_id]["count"]
                }
                for token_query in Utils.tokenize(query)
                if self.index[token_query].get(doc_id)
            ]


    def rank_documents(self, rank):
        """
        Ordonne les documents selon le ranking choisi

        Parameters
        ----------
        rank: Ranking à appliquer pour trier les documents
            str
        """
        if rank == "count":
            sorted_result = sorted(
                self.filtered_documents.items(), 
                key = Utils.rank_count, 
                reverse = True
            )
        elif rank == "stopwords":
            sorted_result = sorted(
                self.filtered_documents.items(), 
                key = Utils.rank_stopwords, 
                reverse = True
            )
        elif rank == "bm25":
            self.compute_params_bm25()

            sorted_result = sorted(
                self.filtered_documents, 
                key = lambda doc: Utils.rank_bm25(doc, self.param_bm25), 
                reverse = True
            )
        
        self.filtered_documents = dict(sorted_result)


    def compute_query_results(self):
        """
        Calcule le résultat de la requête à savoir un dictionnaire avec les documenets triés selon leur rang. 
        Le résultat contient également le nombre total de documents et le nombre de documents après filtrage
        """
        doc_ids = self.filtered_documents.keys()

        for doc in self.documents:
            if str(doc["id"]) in doc_ids:
                self.query_results.append(
                    {
                        "title": doc["title"], 
                        "url": doc["url"]
                    }
                )

        self.query_results.append({
            "metadata": {
                "nb_docs": len(self.documents), 
                "nb_docs_filtered": len(doc_ids)
            }
        })


    def write_results(self, write_path):
        """
        Sauvegarde le résultat de la requête dans un fichier json

        Parameters
        ----------
        write_path: Chemin vers le fichier à sauvegarder
            str
        """
        with open(write_path, "w") as f:
            json.dump(self.query_results, f, ensure_ascii = False, indent = 4)


    def compute_params_bm25(self):
        """
        Calcule les paramètres utilisés pour le score BM25
        """
        # Calcul de |D|
        sum_D = 0
        for doc_id in self.filtered_documents:
            len_D = len(self.documents[doc_id].split(" "))
            self.param_bm25["documents"][doc_id] = len_D
            sum_D += len_D

        # Calcul de mean(|D|)
        self.param_bm25["constant"]["avg_D"] = sum_D / self.param_bm25["constant"]["N"]