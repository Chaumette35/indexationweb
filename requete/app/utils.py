import math

from nltk.corpus import stopwords


class Utils:
    """
    Classe contenant plusieurs fonctions utiles pour l'expension de requêtes et le ranking
    """
    @staticmethod
    def tokenize(q):
        """
        Applique une tokenisation à une requête. 
        Ici, la transformation est un split sur les espaces et une lowerisation

        Parameters
        ----------
        q: Requête à tokeniser
            str
        
        Returns
        -------
            list[str]
        """
        return [word.lower() for word in q.split()]


    @staticmethod
    def rank_count(doc):
        """
        Fonction de ranking renvoyant le nombre de token que contient le document

        Parameters
        ----------
        doc: Document à trier
            (int, dict)
        
        Returns
        -------
            int
        """
        result = 0
        for token_info in doc[1]:
            result += token_info["count"]

        return result


    @staticmethod
    def rank_stopwords(doc):
        """
        Fonction de ranking renvoyant le nombre de token que contient le document et diminue le poids des stop words

        Parameters
        ----------
        doc: Document à trier
            (int, dict)
        
        Returns
        -------
            int
        """
        # On récupère la liste des stop words en Français et en Anglais
        stop_words = stopwords.words("french") + stopwords.words("english")

        result = 0
        for token_info in doc[1]:
            coef = 1
            if token_info["token"] in stop_words:
                # Le poids des stop words est 4 fois moins important
                coef /= 4

            result += coef * token_info["count"]

        return result


    @staticmethod
    def rank_bm25(doc, params):
        """
        Fonction de ranking renvoyant le score BM25 d'un document

        Parameters
        ----------
        doc: Document à trier
            (int, dict)
        
        Returns
        -------
            int
        """
        result = 0

        # Définition des variables
        N = params["constant"]["N"]
        k1 = params["constant"]["k1"]
        b = params["constant"]["b"]
        len_D = params["documents"][doc[0]]
        avg_D = params["documents"]["avg_D"]

        for token_info in doc[1]:
            # f(q_i, D) = count dans notre dictionnaire
            f_q_i_D = token_info["count"]

            n_q_i = params["tokens"][token_info["token"]]

            # Calcul de IDF(q_i)
            idf = math.log(N / n_q_i)

            num = f_q_i_D * (k1 + 1)
            den = f_q_i_D + k1 * (1 - b + b * (len_D / avg_D))

            result += idf * (num / den)

        return result
