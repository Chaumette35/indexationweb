# IndexationWeb

Ce dépôt Gitlab contient, pour l'instant, les trois projets réalisés pour le cours d'Indexation Web à l'ENSAI. Il s'agît : 

 - un crawler, 
 - un index minimal,
 - un systême d'analyse de requêtes

Si vous souhaitez utiliser un des projets, vous devez vous rendre dans le dossier du projet : 

```bash
cd projet_folder
```

Puis, vous n'aurez plus qu'à suivre les instructions données dans les projets. 