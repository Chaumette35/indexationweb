import click
import urllib3

urllib3.disable_warnings()

from app.single_crawler import SingleCrawler
from app.multi_crawler import MultiCrawler



@click.command()
@click.option("--max-url", default = 50, help = "Maximum number of urls saved")
@click.option("--max-links", default = 5, help = "Maximum number of urls added to the frontier per page")
@click.option("--read-sitemap", default = "yes", help = "yes if you want to read sitemaps else no")
@click.option("--use-bdd", default = "yes", help = "yes if you want to use a BDD to store visited websites else no")
@click.option("--num-threads", default = 1, help = "Number of threads")
def main(max_url, max_links, read_sitemap, use_bdd, num_threads):
    # Gestion des paramètres d'entrée
    if read_sitemap == "yes":
        read_sitemap = True
    elif read_sitemap == "no":
        read_sitemap = False
    else:
        raise("Wrong value for read-sitemap (yes or no)")

    if use_bdd == "yes":
        use_bdd = True
    elif use_bdd == "no":
        use_bdd = False
    else:
        raise("Wrong value for read-sitemap (yes or no)")

    if num_threads == 1:
        crawler = SingleCrawler("seed.txt")
    elif not num_threads < 0 and use_bdd:
        crawler = MultiCrawler("seed.txt", num_threads)
    else:
        raise("Wrong value for num-threads or use-bdd. If you want to use a multi-threader crawler, use-bdd must be yes")


    print("Début du crawler\n")

    crawler.run(
        max_urls = max_url, 
        max_links = max_links, 
        read_sitemaps = read_sitemap, 
        use_bdd = use_bdd
    )

    print("\nFin du crawler\n")
    if not use_bdd:
        crawler.write_urls("crawled_webpages.txt")
        print("Enregistrement du fichier crawled_webpages.txt")



if __name__ == "__main__":
    main()