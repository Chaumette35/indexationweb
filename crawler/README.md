# Web Crawler

Ce projet est un crawler développé en Python, conçu pour extraire les URLs en explorant les sites web. 

### Lancer le crawler

Pour lancer le crawler, il faut bien avoir un fichier `seed.txt` à la racine du projet. De plus, si vous soutaitez utiliser une base de données SQL, il vous faudra modifier le fichier `.env` à la racine du projet. 

Tout d'abord, installer les librairies nécessaires pour le projet : 

```bash
pip install -r requirements.txt
```

Ensuite, vous pouvez lancer le crawler avec le fichier `main.py`. Il y a deux paramètres optionnels pour l'exécution : 
 - `max-url` -> Nombre maximal de pages enregistrées par le crawler et qui seront sauvegardées dans un fichier `crawled_webpages.txt`. Valeur par défaut = 50
 - `max-links` -> Nombre maximal de liens d'une même page ajoutés à la frontière. Valeur par défaut = 5
 - `read-sitemap` -> Lire les sitemaps de la page lue. Valeur par défaut = yes. Valeurs possibles yes/no
 - `use-bdd` -> Utiliser une base de données SQL pour sauvegarder les sites visités. Valeur par défaut = yes. Valeurs possibles yes/no
 - `num-threads` -> Nombre de threads utilisés pour crawler le web. Valeur par défaut = 1. Valeurs possibles entier strictement positif

Cela donne donc : 

```bash
python3 main.py --max-url=50 --max-links=5 --read-sitemap=yes --use-bdd=yes --num-threads=5
```

### Fonctionnement du crawler

Le but du crawler est de récupérer les URLs présentes sur des sites web. 

 - Tout d'abord, le crawler récupère les URLs présentes dans la seed et les ajoutent à la frontière. 
 - Il trouve les autres URLs présentes grâce à la librairie BeautifulSoup en cherchant les balises `a` dans l'html du site. 
    - Il veille à ne prendre que les URLs autorisées par le site. Pour cela, il utilise la classe `RobotFileParser` de la librairie urllib qui lit le fichier robots.txt pour le User-Agent `*`
    - Il veille également à ne pas relire un site déjà parser. Pour cela, on sauvegarde tous les sites déjà parser dans un `set`. 
    - De plus, si `read-sitemap` est yes alors le crawler utilise `sitemap_tree_for_homepage` de la librairie ultimate-sitemap-parser pour ajouter toutes les sitemaps d'un site à la frontière. 
 - Dès que le crawler a trouvé `max-links` nouvelles URLs, il les ajoutent à la frontière puis il y a deux possibilitées en fonction du paramètre `use-bdd` : 
    - Si `use-bdd=yes` alors l'URL et l'âge de la page sont ajoutées à votre base de données SQL. L'âge est incrémenté de 1 chaque fois qu'une nouvelle page est visitée. L'âge ne peut pas revenir à 0 car, avec nos contraintes, il est impossible de visiter deux fois un site. 
    - Si `use-bdd=np` alors le site parsé est ajouté à une liste `good_visited`. 
 - Pour l'arrêt du crawler, il y a également deux possibilitées en fonction de `use-bdd` : 
    - Si `use-bdd=yes` alors le crawler ne s'arrêtera jamais sauf s'il n'y plus d'éléments dans la frontière. Il est également possible d'arrêter le crawler manuellement avec `crtl + C`. 
    - Si `use-bdd=no` alors le crawler s'arrêtera lorsque `good_visited` a `max-urls` URLs,  puis le crawler sauvegardera le contenu de `good_visited` dans un fichier `crawled_webpages.txt`. 

 - Lorsque `num-threads` > 1, un multi-threader crawler se lance. Pour chaque crawler, le fonctionnement est globalement le même sauf lorsque la frontière est vide, les crawlers sont mis en attente car potentiellement un autre crawler va ajouter des URLs. La condition d'arrêt est donc différente, ici le multi-threader crawler s'arrête lorsque tous les crawlers sont en attente d'ajout dans la frontière. Il est également possible d'arrêter le multi-threader crawler manuellement en appuyant plusieurs fois sur `crtl + C`. 

**Attention:** <span style="color:red">Pour utiliser le multi-threader crawler, il faut nécessairement utiliser une base de données !</span> 

### Contributeur

Maël CHAUMETTE