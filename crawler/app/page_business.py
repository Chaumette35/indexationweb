from pydantic import BaseModel

# Finalement, la classe n'est pas très utile dans l'état.
# Si l'on sauvegardait plus d'informations sur les pages, cela aurait plus d'intérêt. 
class PageBusiness(BaseModel):
    """
    Modèle de base définissant une page dans la base de données

    Attributes
    ----------
    url: URL de la page
        str
    age: Age de la page (commence à -1 car l'âge de la page ajoutée est directement incrémenté)
        int
    """
    url: str = ""
    age: int = -1