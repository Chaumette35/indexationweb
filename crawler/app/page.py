import ssl
import time
import logging
import requests

from urllib.parse import urlparse
from urllib.robotparser import RobotFileParser

from usp.tree import sitemap_tree_for_homepage

from bs4 import BeautifulSoup

from app.utils import Utils
from app.page_dao import PageDAO
from app.page_business import PageBusiness


# Empêche certains problèmes lors de la requête à un site internet
ssl._create_default_https_context = ssl._create_unverified_context

# Empêche l'affiche des informations données par usp (lors de la lecture des sitemaps)
logging.getLogger("usp.helpers").setLevel(logging.CRITICAL)
logging.getLogger("usp.fetch_parse").setLevel(logging.CRITICAL)


class Page:
    """
    Classe définissant une page visitée par le crawler. 
    Implémente différentes méthodes utiles pour l'accès aux données de la page web. 

    Attributes
    ----------
    url: URL de la page visitée
        str
    to_add: Nombre de liens à ajoutés dans la frontière
        int
    robot: Informations disponibles dans robots.txt
        RobotFileParser
    is_multi: True si le crawler est un multi-threader crawler et False sinon
        bool
    result: Résultat de l'obtiention des liens sur la page visitée
        bool
    result_bdd: Résultat de l'ajout de la page à la base de données
        bool
    """
    def __init__(self, url, to_add, is_multi = False):
        """
        Initializer

        Parameters
        ----------
        url: URL de la page à crawler
            str
        to_add: Nombre de liens à ajoutés dans la frontière
            int
        is_multi: True si le crawler est un multi-threader crawler et False sinon
            bool
        """
        self.url = Utils.correct_name(url)
        self.to_add = to_add
        self.robot = self.init_robot()

        self.is_multi = is_multi
        self.result = True
        self.result_bdd = True


    def give_web_links(self, doc):
        """
        Récupère un certain nombre de liens depuis la page. 
        Renvoie la liste des URLs si la lecture s'est passé sans problème et renvoie None sinon

        Parameters
        ----------
        doc: Ensemble des URLs de la frontière et des sites déjà visités
            set
        
        Returns
        -------
            Union[list[str], None]
        """
        result = []

        try:
            page = requests.get(self.url, verify = False)
        except:
            self.result = False
            return None
        
        # Certains sites, comme twitter, n'autorisent aucune lecture, 
        # La page ne peut donc pas être lu
        if not self.check_robot(self.url):
            self.result = False
            return None
        
        soup = BeautifulSoup(page.text, "html.parser")

        # On récupère l'ensemble des liens de la page
        links = [link.get("href") for link in soup.find_all("a") if Utils.valid_url(link.get("href"))]
        while self.to_add > 0 and links:
            link = Utils.correct_name(links.pop(0))

            # On regarde si le lien n'est pas déjà passé et s'il peut être lu
            if not link in doc and self.check_robot(link):
                result.append(link)
                self.to_add -= 1

        # On attend 5s pour respecter la politeness
        time.sleep(5)

        return result


    def give_sitemap_links(self, doc_visited, sitemap_visited):
        """
        Récupère l'ensemble des sitemaps de la page

        Parameters
        ----------
        doc_visited: Ensemble des URLs de la frontière et des sites déjà visités
            set
        sitemap_visited: Ensemble des URLs de base déjà visités 
        (par exemple, si l'on a obtenu les sitemaps de ensai.fr alors on ne relira pas les sitemaps de ensai.fr/exemple)
            set
        
        Returns
        -------
            list[str]
        """
        result = []

        parse = urlparse(self.url)
        base_url = f"{parse.scheme}://{parse.netloc}/"
        if self.url not in sitemap_visited:
            # On utilise usp pour récupérer l'ensemble des sitemaps
            tree = sitemap_tree_for_homepage(base_url)

            for page in tree.all_pages():
                if page.url not in set(result).union(doc_visited):
                    result.append(page.url)

        return result


    def init_robot(self):
        """
        Initialise la lecture du robots.txt et renvoie un RobotFileParser 
        pour accéder au informations de robots.txt facilement. 

        Returns
        -------
            RobotFileParser
        """
        parse = urlparse(self.url)
        base_url = f"{parse.scheme}://{parse.netloc}"
        rp = RobotFileParser(base_url + "/robots.txt")
        rp.read()

        return rp


    def check_robot(self, new_url):
        """
        Permet de savoir si une nouvelle URL peut être crawler depuis notre page

        Parameters
        ----------
        new_url: Nouvelle URL que l'on aimerait bien lire
            str
        
        Returns
        -------
            bool
        """
        return self.robot.can_fetch("*", new_url)


    def add_page_bdd(self):
        """
        Ajoute la page à une base de données. Renvoie True si l'ajout s'est bien passé et False sinon. 

        Returns
        -------
            bool
        """
        page = PageBusiness(url = self.url, age = -1)

        result = PageDAO().add_page(page)
        if result:
            return True
        else:
            self.result_bdd = False
            return False


    def print_result(self, worker_id, use_bdd):
        """
        Fonction pour afficher le résultat d'une lecture de la page. 

        Parameters
        ----------
        worker_id: Numéro du crawler dans le cas du multi-threading
            int
        use_bdd: True si l'utilisateur utilise une une base de données et False sinon
            bool
        """
        message = ""

        if self.is_multi:
            message += f"Travail de worker {worker_id}\n"

        message += f"Lecture de : {self.url}\n"
        message += f"             -> {'Succès' if self.result else 'Echec'}\n"

        if use_bdd and self.result:
            message += f"             -> {'Ajout à la bdd' if self.result_bdd else '''Echec de l'ajout'''}\n"

        print(message)