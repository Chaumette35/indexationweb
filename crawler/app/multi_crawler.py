import time
import threading

from app.page import Page
from app.page_dao import PageDAO

from concurrent.futures import ThreadPoolExecutor, as_completed


class MultiCrawler:
    """
    Classe correspondant à un multi-treader crawler. 

    Attributes
    ----------
    frontier: Ensemble des URLs qui sont encore à lire
        list[str]
    good_visited: Ensemble des URLs qui sont visitables par le futur index
        list[str]
    all_visited: Ensemble des URLs qui ont ont été visitées par le crawler
        set
    sitemaps_visited: Ensemble des URLs de base déjà visitées avec des sitemaps
        set
    num_threads: Nombre de crawlers travaillant en parallèle
        int
    workers_done: Liste de états des crawlers
        list[Event]
    """
    def __init__(self, path_seed, num_threads):
        """
        Initializer

        Parameters
        ----------
        path_seed: Chemin vers seed.txt contenant les premiers sites à crawler
            str
        num_threads: Nombre de crawlers travaillant en parallèle
            int
        """
        self.frontier = []
        self.good_visited = []
        self.all_visited = set()
        self.sitemaps_visited = set()

        self.num_threads = num_threads
        self.workers_done = None

        # Initialisation de la frontière
        with open(path_seed, 'r') as f:
            self.frontier = [line.rstrip('\n') for line in f]

        # Initialisation de la base de données
        PageDAO().init_bdd()


    def write_urls(self, path_save):
        """
        Méthode permettant l'écriture d'un fichier contenant tous les éléments de good_visited. 

        Parameters
        ----------
        path_save: Chemin vers le fichier à sauvegarder
            str
        """
        with open(path_save, 'w') as f:
            for url in self.good_visited:
                f.write(f"{url}\n")


    def worker(self, id, max_links, read_sitemaps, use_bdd):
        """
        Méthode définissant le comportement d'un crawler. 

        Parameters
        ----------
        id: Numéro du crawler
            int
        max_links: Nombre maximal de liens à lire par page
            int
        read_sitemaps: True si l'utilisateur veut ajouter les sitemaps à la frontière et False sinon
            bool
        use_bdd: True si l'utilisateur utilise une base de données pour sauvegarder les pages visitées
            bool
        """
        while True:
            # Un crawler "travaille" tout le temps sauf lorsque la frontière est vide
            if not self.frontier:
                # Lorsque c'est le cas, on regarde si tous les crawlers sont en attentes
                # Si c'est le cas alors le multi-threader crawler s'arrête
                # Sinon, le crawler attend 10s puis continue à "travailler"
                if all(worker_done.is_set() for worker_done in self.workers_done):
                    break
                else:
                    self.workers_done[id].set()
                    time.sleep(10)
                    continue
            
            self.workers_done[id].clear()

            # Le fonctionnement suivant est exactement le même que pour un SingleCrawler
            link = self.frontier.pop(0)
            page = Page(link, max_links, True)

            self.all_visited.add(link)

            links_to_visit = page.give_web_links(set(self.frontier).union(self.all_visited))
            if links_to_visit is not None:
                self.frontier += links_to_visit

                if use_bdd:
                    page.add_page_bdd()
                    PageDAO().incr_age()
                else:
                    self.good_visited.append(link)

            if read_sitemaps:
                links_to_visit = page.give_sitemap_links(set(self.frontier).union(self.all_visited), self.sitemaps_visited)
                self.frontier += links_to_visit
                self.sitemaps_visited.add(link)

            page.print_result(id, use_bdd)


    def run(self, max_urls, max_links, read_sitemaps, use_bdd):
        """
        Méthode contrôlant la mise en route du crawler. 

        Parameters
        ----------
        max_urls: Nombre maximal d'URLs à lire avant le sauvegarder les URLs dans un fichier
        (Inutile dans notre cas, car le multi-threader crawler sauvegarde les pages dans une base de données)
            int
        max_links: Nombre maximal de liens à lire par page
            int
        read_sitemaps: True si l'utilisateur veut ajouter les sitemaps à la frontière et False sinon
            bool
        use_bdd: True si l'utilisateur utilise une base de données pour sauvegarder les pages visitées
            bool
        """
        self.workers_done = [threading.Event() for _ in range(self.num_threads)]

        with ThreadPoolExecutor(max_workers = self.num_threads) as executor:
            # Lancement des crawlers avec la méthode submit de ThreadPoolExecutor
            crawlers = [
                # Chaque crawler effectue la méthode worker
                executor.submit(self.worker, i, max_links, read_sitemaps, use_bdd)
                for i in range(self.num_threads)
            ]

            # Patie lorsqu'un ou plusieurs crawlers ont fini
            for i, crawler in enumerate(as_completed(crawlers)):
                try:
                    # S'il n'y a pas eu d'erreur ils retournent le résultat de la fonction self.worker
                    crawler.result()
                except Exception as e:
                    # S'il y a eu une erreur lors de l'exécution de self.worker alors un message s'affiche
                    print(f"Le crawler {i} s'est arrêté à cause de : \n{e}")