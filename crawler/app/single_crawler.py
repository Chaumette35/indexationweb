from app.page import Page
from app.page_dao import PageDAO


class SingleCrawler:
    """
    Classe correspondant à un single-treader crawler. 

    Attributes
    ----------
    frontier: Ensemble des URLs qui sont encore à lire
        list[str]
    good_visited: Ensemble des URLs qui sont visitables par le futur index
        list[str]
    all_visited: Ensemble des URLs qui ont ont été visitées par le crawler
        set
    sitemaps_visited: Ensemble des URLs de base déjà visitées avec des sitemaps
        set
    """
    def __init__(self, path_seed):
        """
        Initializer

        Parameters
        ----------
        path_seed: Chemin vers seed.txt contenant les premiers sites à crawler
            str
        """
        self.frontier = []
        self.good_visited = []
        self.all_visited = set()
        self.sitemaps_visited = set()

        # Initialisation de la frontière
        with open(path_seed, 'r') as f:
            self.frontier = [line.rstrip('\n') for line in f]

        # Initialisation de la base de données
        PageDAO().init_bdd()


    def write_urls(self, path_save):
        """
        Méthode permettant l'écriture d'un fichier contenant tous les éléments de good_visited. 

        Parameters
        ----------
        path_save: Chemin vers le fichier à sauvegarder
            str
        """
        with open(path_save, 'w') as f:
            for url in self.good_visited:
                f.write(f"{url}\n")


    def run(self, max_urls, max_links, read_sitemaps, use_bdd):
        """
        Méthode contrôlant la mise en route du crawler. 

        Parameters
        ----------
        max_urls: Nombre maximal d'URLs à lire avant le sauvegarder les URLs dans un fichier
            int
        max_links: Nombre maximal de liens à lire par page
            int
        read_sitemaps: True si l'utilisateur veut ajouter les sitemaps à la frontière et False sinon
            bool
        use_bdd: True si l'utilisateur utilise une base de données pour sauvegarder les pages visitées
            bool
        """
        # Tant que la frontière n'est pas vide et que good_visited n'a pas trop de sites le crawler continue
        while self.frontier and len(self.good_visited) < max_urls:

            link = self.frontier.pop(0)
            page = Page(link, max_links)

            self.all_visited.add(link)

            # Les fichiers à ne pas rajouter sont ceux dans la frontière et ceux déjà visités
            links_to_visit = page.give_web_links(set(self.frontier).union(self.all_visited))
            if links_to_visit is not None:
                self.frontier += links_to_visit

                # Si l'utilisateur a une base de données alors on n'a rien dans good_visited
                # donc le crawler ne s'arrêtera que lorsqu'il n'y a plus rien dans la frontière
                if use_bdd:
                    page.add_page_bdd()
                    PageDAO().incr_age()
                else:
                    self.good_visited.append(link)
            
            if read_sitemaps:
                links_to_visit = page.give_sitemap_links(set(self.frontier).union(self.all_visited), self.sitemaps_visited)
                self.frontier += links_to_visit
                self.sitemaps_visited.add(link)

            page.print_result(0, use_bdd)