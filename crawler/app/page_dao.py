from app.db_connection import DBConnection
from app.page_business import PageBusiness


class PageDAO:
    """
    Classe gérant la base de données. 
    """
    def init_bdd(self):
        """
        Initialise la base de données en créant la table utilisée pour sauvegarder les pages visitées. 
        """
        request = """
        CREATE SCHEMA IF NOT EXISTS crawler;

        DROP TABLE IF EXISTS crawler.page;

        CREATE TABLE crawler.page(
            url TEXT PRIMARY KEY, 
            age NUMERIC
        );
        """

        with DBConnection().connection as connection:
            with connection.cursor() as cursor:
                cursor.execute(request)


    def add_page(self, page: PageBusiness):
        """
        Ajoute une page à la base de données. Renvoie True si l'ajout s'est bien passé et False sinon. 

        Parameters
        ----------
        page: Page à ajouter à la base de données
            PageBusiness

        Returns
        -------
            bool
        """
        with DBConnection().connection as connection:
            with connection.cursor() as cursor:
                cursor.execute(
                    "INSERT INTO crawler.page (url, age) "\
                    "VALUES (%(url)s, -1) "\
                    "RETURNING url;"
                    , {"url": page.url}
                )
                res = cursor.fetchone()
        
        if res:
            return True
        else:
            return False


    def incr_age(self):
        """
        Incrémente l'âge de toutes pages de la base de données. 
        """
        with DBConnection().connection as connection:
            with connection.cursor() as cursor:
                cursor.execute(
                    "UPDATE crawler.page "\
                    "SET age = age + 1"
                )


    # Dans la suite les fonctions ne sont pas utiles pour le crawler actuel mais pourraient l'être
    def find_all_pages(self, limit, offset):
        """
        Cherche et renvoie toutes les pages de la base de données avec une limite et un offset. 

        Parameters
        ----------
        limit: Nombre maximal de pages à renvoyer
            int
        offset: Décalage à appliquer sur la recherche
            int
        
        Returns
        -------
            list[PageBusiness]
        """
        request = "SELECT * FROM crawler.page "

        if limit:
            request += f"LIMIT {limit}"
        if offset:
            request += f"OFFSET {offset}"

        with DBConnection().connection as connection:
            with connection.cursor() as cursor:
                cursor.execute(request)
                res = cursor.fetchall()
        
        pages = []
        if res:
            for row in res:
                pages.append(PageBusiness(url = row["url"], age = row["age"]))
        
        return pages
    

    def find_demande(self, url):
        """
        Récupère une certaine page selon son URL. 

        Parameters
        ----------
        url: URL de la page à récupérer
            str
        
        Returns
        -------
            Union[PageBusiness, None]
        """
        with DBConnection().connection as connection:
            with connection.cursor() as cursor:
                cursor.execute(
                    "SELECT * FROM crawler.page "\
                    "WHERE url=%(url)s"
                    , {"url": url}
                )
                res = cursor.fetchone()
        
        if res:
            return PageBusiness(url = res["url"], age = res["age"])
        else:
            return None