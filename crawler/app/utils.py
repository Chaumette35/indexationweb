import validators


class Utils:

    @staticmethod
    def correct_name(url):
        if url.endswith("/"):
            return url
        else:
            return url + "/"


    @staticmethod
    def valid_url(url):
        return bool(validators.url(url))