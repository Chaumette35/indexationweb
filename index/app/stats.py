import json


class Stats:
    """
    Classe pour calculer les statistiques associés à un Index

    Attributes
    ----------
    index: index dont on souhaite avoir les statistiques
        Index
    """
    def __init__(self, index):
        """
        Initializer

        Parameters
        ----------
        index: index dont on souhaite avoir les statistiques
            Index
        """
        self.index = index


    def nb_docs(self):
        """
        Calcule le nombre de documents dans l'index

        Returns
            int
        """
        return len(self.index.documents)


    def nb_tokens(self):
        """
        Calcule le nombre de tokens dans les documents

        Returns
        -------
            int
        """
        result = 0
        for v in self.index.inv_index.values():
            result += len(v)
        
        return result


    def moy_tokens(self):
        """
        Calcule le nombre moyenne de tokens dans l'ensemble des documents

        Returns
        -------
            float
        """
        return self.nb_tokens() / self.nb_docs()


    def write_stats(self, write_path):
        """
        Sauvegarde les statistiques déjà calculées

        Parameters
        ----------
        write_path: chemin vers le fichier à sauvegarder
            str
        """
        doc = {
            "nb_docs": self.nb_docs(), 
            "nb_tokens": self.nb_tokens(), 
            "moy_tokens": self.moy_tokens()
        }

        with open(write_path, 'w', encoding = 'utf-8') as f:
            json.dump(doc, f, ensure_ascii = False)


    def show_stats(self):
        """
        Affiche les statistiques calculées
        """
        print("\n-------------------------------------------")
        print("Statistiques sur l'ensemble des documents : ")
        print("-------------------------------------------\n")
        print(f"Il y a {self.nb_docs()} documents", end = " ")
        print(f"et {self.nb_tokens()} tokens")
        print(f"En moyenne, il y a {round(self.moy_tokens(), 2)} tokens par document\n")