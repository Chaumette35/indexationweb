class Utils:
    """
    Classe contenant des fonctions utiles pour le calcul des index
    """
    @staticmethod
    def valide(s):
        """
        Renvoie True si le string en entrée est un mot valide et False sinon

        Parameters
        ----------
        s: String dont on souhaite savoir s'il s'agît d'un mot valide
            str
        
        Returns
        -------
            bool
        """
        return s.isalpha()