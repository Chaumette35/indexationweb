import json
import string

from nltk.stem.snowball import SnowballStemmer

from app.utils import Utils

class Index:
    """
    Classe pour calculer l'index d'un fichier contenant des sites web crawlés

    Attributes
    ----------
    documents: dictionnaire avec les résultats d'un crawler
        dict[str: str]
    inv_index: dictionnaire des index non positionnels
        dict[str: list[int]]
    inv_stem: dictionnaire où les clés de inv_index sont stemmées pour garder le corps de base du token
        dict[str: list[int]]
    inv_index_pos: dictionnaire des index positionnels
        dict[str: dict[int: list[int]]]
    """
    def __init__(self, path_doc):
        """
        Initializer

        Parameters
        ----------
        path_doc: chemin vers le fichier des résultats du crawler
            str
        """
        self.documents = self.init_documents(path_doc)
        self.inv_index = {}
        self.inv_stem = {}
        self.inv_index_pos = {}


    def init_documents(self, path_doc):
        """
        Récupère les données du crawler

        Parameters
        ----------
        path_doc: chemin vers le fichier des résultats du crawler
            str

        Returns
        -------
            dict[str: str]
        """
        with open(path_doc, 'r') as f:
            data = json.load(f)

        return data


    def compute_index_title(self):
        """
        Calcule l'index non positionnel à partir des documents

        Returns
        -------
            dict[str: list[int]]
        """
        for i, title in enumerate(self.documents):
            words = [
                mot.strip(string.punctuation).lower() # Transformer les mots en token
                for mot in title["title"].split() # Sépare les mots du titre des documents
                if Utils.valide(mot.strip(string.punctuation)) # Garde seulement les tokens valides
            ]
            # On ajoute chaque token à notre dictionnaire d'index non positionnel
            for word in words:
                if word in self.inv_index:
                    self.inv_index[word].append(i)
                else:
                    self.inv_index[word] = [i]

        return self.inv_index


    def compute_stem_title(self):
        """
        Calcule l'index non positionnel mais, ici, les tokens sont stemmés, 
        c'est-à-dire, on garde seulement le corps de base des tokens

        Returns
        -------
            dict[str: list[int]]
        """
        # Stemmer de nltk
        stemmer = SnowballStemmer("french")
        
        # On effectue la même opération que pour calculer l'index non positionnel
        for i, title in enumerate(self.documents):
            words = [
                mot.strip(string.punctuation).lower()
                for mot in title["title"].split()
                if Utils.valide(mot.strip(string.punctuation))
            ]
            for word in words:
                # On stemme les tokens
                stem_word = stemmer.stem(word)
                if stem_word in self.inv_stem:
                    self.inv_stem[stem_word].append(i)
                else:
                    self.inv_stem[stem_word] = [i]

        return self.inv_stem


    def compute_pos_index_title(self):
        """
        Calcule l'index positionnel à partir des documents

        Returns
        -------
            dict[str: dict[int: list[int]]]
        """
        # On fait la même opération que dans les deux autres méthodes
        for i, title in enumerate(self.documents):
            words = [
                mot.strip(string.punctuation).lower() 
                for mot in title["title"].split() 
                if Utils.valide(mot.strip(string.punctuation))
            ]
            # On parcourt les tokens en gardant la position des tokens
            for j, word in enumerate(words):
                if word in self.inv_index_pos and i in self.inv_index_pos[word]:
                    self.inv_index_pos[word][i].append(j)
                elif word in self.inv_index_pos:
                    self.inv_index_pos[word][i] = [j]
                else:
                    self.inv_index_pos[word] = {i: [j]}

        return self.inv_index_pos


    def write_index(self, write_path):
        """
        Sauvegarde les résultats de l'index non positionnel

        Parameters
        ----------
        write_path: chemin vers le fichier à sauvegarder
            str
        """
        with open(write_path, 'w', encoding = 'utf-8') as f:
            json.dump(self.inv_index, f, ensure_ascii = False, indent = 4)


    def write_stem(self, write_path):
        """
        Sauvegarde les résultats du stemmer non positionnel

        Parameters
        ----------
        write_path: chemin vers le fichier à sauvegarder
            str
        """
        with open(write_path, 'w', encoding = 'utf-8') as f:
            json.dump(self.inv_stem, f, ensure_ascii = False, indent = 4)


    def write_index_pos(self, write_path):
        """
        Sauvegarde les résultats de l'index positionnel

        Parameters
        ----------
        write_path: chemin vers le fichier à sauvegarder
            str
        """
        with open(write_path, 'w', encoding = 'utf-8') as f:
            json.dump(self.inv_index_pos, f, ensure_ascii = False, indent = 4)