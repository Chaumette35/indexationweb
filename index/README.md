# Index Minimal

Ce projet est un index développé en Python, conçu pour analyser les informations obtenues par un crawler. 

### Lancer l'index

Pour lancer l'index, il faut avoir un fichier json avec l'ensemble des URLs crawlées à la racine du projet. Dans ce fichier, il y a également le titre, le contenu du site web et enfin le titre du premier paragraphe. 

Tout d'abord, installer les librairies nécessaires pour le projet : 

```bash
pip install -r requirements.txt
```

Ensuite, vous pouvez lancer l'index avec le fichier `main.py`. Si vous souhaitez utiliser un stemmer pour améliorer l'analyse alors vous pouvez rajouter le paramètre `use-stem` (Valeurs possibles yes/no). Si vous souhaitez avec un index positionnel, c'est-à-dire, avoir la position des tokens dans un document alors vous pouvez rajouter le paramètre `use-pos` (Valeurs possibles yes/no). 

Cela donne donc : 

```bash
python3 main.py --use-stem=yes --use-pos=yes
```

### Fonctionnement de l'index

Le but de l'index est d'analyser les informations présentes sur des sites web. 

 - Tout d'abord, l'index récupère le titre de chaque site web et le tokenise. Puis, il vérifie que ces tokens sont biens valides (qu'il s'agissent bien de tokens alphanumériques). 
 - Le résultat fournit par l'index est un dictionnaire où les clés sont les tokens présents dans les titres et les valeurs sont les indices des documents où apparaissent les différents tokens. 
 - Un stemmer est utilisé sur les clés du résultat précédent pour garder uniquement le corps de base du token. Ce stemmer, le `SnowballStemmer` provient de la librairie `nltk`, il possède, contrairement au autre stemmer de la librairie, un vocabulaire français. 
 - Un index positionnel calcule la position des tokens dans les documents. Le résultat est un dictionnaire où les clés sont les tokens et les valeurs sont un autre dictionnaire où les clés sont les indices des documents et les valeurs sont les listes des positions du token dans le document. 
 - L'index fournit également des statistiques tels que le nombre de documents, le nombre de tokens total et la moyenne des tokens par document. 
 - A la fin, les résultats sont sauvegardés dans le dossier `results` et dans différents fichiers json : 
    - les résultats de l'index sont sauvegardés dans le fichier `title.non_pos_index.json`, 
    - ceux du stemmer sont sauvegardés dans le fichier `mon_stemmer.title.non_pos_index.json`, 
    - les index positionnels sont sauvegardés dans le fichier `title.pos_index.json`, 
    - et les statistiques sont sauvegardées dans le fichier `metadata.json`. 

### Contributeur

Maël CHAUMETTE