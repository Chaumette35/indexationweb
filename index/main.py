import click

from app.index import Index
from app.stats import Stats


@click.command()
@click.option("--use-stem", default = "yes", help = "Valeurs possibles (yes/no)")
@click.option("--use-pos", default = "yes", help = "Valeurs possibles (yes/no)")
def main(use_stem, use_pos):
    if use_stem == "yes":
        use_stem = True
    elif use_stem == "no":
        use_stem = False
    else:
        raise("Wrong value for use-stem (yes/no)")
    
    if use_pos == "yes":
        use_pos = True
    elif use_pos == "no":
        use_pos = False
    else:
        raise("Wrong value for use-pos (yes/no)")


    index = Index("crawled_urls.json")
    print("Début de l'Index\n----------------\n")

    index.compute_index_title()
    index.write_index("results/title.non_pos_index.json")
    print("Enregistrement du fichier results/title.non_pos_index.json")


    if use_stem:
        index.compute_stem_title()
        index.write_stem("results/mon_stemmer.title.non_pos_index.json")
        print("Enregistrement du fichier results/mon_stemmer.title.non_pos_index.json")
    

    if use_pos:
        index.compute_pos_index_title()
        index.write_index_pos("results/title.pos_index.json")
        print("Enregistrement du fichier results/title.pos_index.json")


    stats = Stats(index)
    stats.show_stats()
    stats.write_stats("results/metadata.json")
    print("Enregistrement du fichier results/metadata.json")


if __name__ == "__main__":
    main()